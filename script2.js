//carga de las hojas dentro del dynamic-content
document.addEventListener("DOMContentLoaded", function () {
    const links = document.querySelectorAll(".nav-links a[data-page], .nav-links span[data-page]"
    );

    links.forEach((link) => {
        link.addEventListener("click", function (event) {
            event.preventDefault();

            const page = this.getAttribute("data-page");
            loadPage(page);
        });
    });
});

function loadPage(page) {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", page, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            document.getElementById("dynamic-content").innerHTML = xhr.responseText;
        }
    };
    xhr.send();
}
